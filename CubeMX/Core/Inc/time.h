/*
 * time.h
 *
 *  Created on: May 30, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "stdint.h"

#ifndef INC_TIME_H_
#define INC_TIME_H_

uint32_t Get_us_Since(const uint32_t time_stamp);

uint32_t Get_us_Since_And_Update(uint32_t* time_stamp);

#endif /* INC_TIME_H_ */
