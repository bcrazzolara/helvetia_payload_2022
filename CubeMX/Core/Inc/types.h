/*
 * types.h
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

#ifndef INC_TYPES_H_
#define INC_TYPES_H_

typedef enum {
    SPI_INTERFACE_1 = 0u,
    SPI_INTERFACE_2 = 1u
} SPI_INTERFACE;

typedef enum {
    SENSOR_1 = 0u,
    SENSOR_2 = 1u,
    SENSOR_3 = 2u,
    SENSOR_4 = 3u,
    SENSOR_5 = 4u
} SENSOR;

typedef enum {
    SPI_TRANSFER_OK    = 0u,
    SPI_TRANSFER_ERROR = 1u
} SPI_TRANSFER_STATUS;

typedef enum {
    FALSE = 0u,
    TRUE  = 1u
} BOOL;

#endif /* INC_TYPES_H_ */
