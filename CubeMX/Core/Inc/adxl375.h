/*
 * adxl375.h
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "types.h"
#include "stm32f4xx_hal.h"

#ifndef INC_ADXL375_H_
#define INC_ADXL375_H_

BOOL Check_Sensor_Timeout(const uint32_t last_read, const uint32_t time_now);

void Configure_ADXL375(const SENSOR sensor);

#endif /* INC_ADXL375_H_ */
