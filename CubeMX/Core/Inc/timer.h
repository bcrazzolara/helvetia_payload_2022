/*
 * timer.h
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

#ifndef INC_TIMER_H_
#define INC_TIMER_H_

void TIM2_Init(void);

#endif /* INC_TIMER_H_ */
