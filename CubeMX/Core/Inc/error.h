/*
 * error.h
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

#ifndef INC_ERROR_H_
#define INC_ERROR_H_

void Error_Handler(void);

#endif /* INC_ERROR_H_ */
