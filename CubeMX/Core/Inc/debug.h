/*
 * debug.h
 *
 *  Created on: May 30, 2022
 *      Author: Felix Crazzolara
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

int __io_putchar(int ch);

int _write(int file, char *ptr, int len);

#endif /* INC_DEBUG_H_ */
