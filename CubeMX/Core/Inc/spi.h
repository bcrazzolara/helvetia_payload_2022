/*
 * spi.h
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "stm32f4xx_hal.h"
#include "types.h"

#ifndef INC_SPI_H_
#define INC_SPI_H_

void SPI_Tranceive(const uint8_t* pTxData, uint8_t* pRxData, const uint16_t size, SPI_TypeDef* spi);

void SPI_Read_Write(uint8_t reg, const uint8_t val, uint8_t* ret, const SPI_INTERFACE spi_interface, const SENSOR sensor,
                    const BOOL read);

void SPI_Read(const uint8_t reg, uint8_t* ret, const SPI_INTERFACE spi_interface, const SENSOR sensor);

void SPI_Multi_Read(uint8_t reg0, uint8_t* ret, const uint8_t num_regs, const SPI_INTERFACE spi_interface,
                    const SENSOR sensor);

void SPI_Write(const uint8_t reg, const uint8_t val, const SPI_INTERFACE spi_interface, const SENSOR sensor);

#endif /* INC_SPI_H_ */
