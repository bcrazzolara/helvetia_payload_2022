/*
 * defines.h
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

#ifndef INC_DEFINES_H_
#define INC_DEFINES_H_

/* Pin, port & EXTI definitions */
#define SS_S1_Pin       GPIO_PIN_14
#define SS_S1_GPIO_Port GPIOC

#define SS_S2_Pin       GPIO_PIN_11
#define SS_S2_GPIO_Port GPIOD

#define SS_S3_Pin       GPIO_PIN_6
#define SS_S3_GPIO_Port GPIOC

#define DR_S1_Pin       GPIO_PIN_15
#define DR_S1_GPIO_Port GPIOC
#define DR_S1_EXTI_IRQn EXTI15_10_IRQn

#define DR_S2_Pin       GPIO_PIN_10
#define DR_S2_GPIO_Port GPIOD
#define DR_S2_EXTI_IRQn EXTI15_10_IRQn

#define DR_S3_Pin       GPIO_PIN_7
#define DR_S3_GPIO_Port GPIOC
#define DR_S3_EXTI_IRQn EXTI9_5_IRQn

#define SS_SD_Pin       GPIO_PIN_0
#define SS_SD_GPIO_Port GPIOC

#define BUTTON_Pin       GPIO_PIN_0
#define BUTTON_GPIO_Port GPIOA

#define LED_RED_Pin       GPIO_PIN_14
#define LED_RED_GPIO_Port GPIOD

#define LED_BLUE_Pin       GPIO_PIN_15
#define LED_BLUE_GPIO_Port GPIOD

/* ADXL375 register addresses */
#define DEVID_REG_ADDR      0x00
#define BW_RATE_REG_ADDR    0x2C
#define POWER_CTL_REG_ADDR  0x2D
#define INT_ENABLE_REG_ADDR 0x2E
#define INT_MAP_REG_ADDR    0x2F

/* FATFS configuration */
#define SD_SPI_HANDLE hspi2
#define SD_SPI        SPI2

#endif /* INC_DEFINES_H_ */
