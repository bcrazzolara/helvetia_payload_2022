/*
 * gpio.h
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "stm32f407xx.h"
#include "types.h"

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

void GPIO_Set(GPIO_TypeDef* port, const uint16_t GPIO_Pin);

void GPIO_Reset(GPIO_TypeDef* port, const uint16_t GPIO_Pin);

BOOL GPIO_Read(GPIO_TypeDef* port, const uint16_t GPIO_Pin);

#endif /* INC_GPIO_H_ */
