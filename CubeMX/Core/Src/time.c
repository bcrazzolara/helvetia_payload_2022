/*
 * time.c
 *
 *  Created on: May 30, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "time.h"
#include "stm32f407xx.h"

uint32_t Get_us_Since(const uint32_t time_stamp) {
    const uint32_t time_now = TIM2->CNT;
    if (time_now < time_stamp) { // time_now + 100'000'000 >= time_stamp
        return (time_now + 100000000u) - time_stamp;
    } else {
        return time_now - time_stamp;
    }
}

uint32_t Get_us_Since_And_Update(uint32_t* time_stamp_ptr) {
    const uint32_t time_stamp = *time_stamp_ptr;
    const uint32_t time_now   = TIM2->CNT;

    *time_stamp_ptr = time_now;

    if (time_now < time_stamp) { // time_now + 100'000'000 >= time_stamp
        return (time_now + 100000000u) - time_stamp;
    } else {
        return time_now - time_stamp;
    }
}
