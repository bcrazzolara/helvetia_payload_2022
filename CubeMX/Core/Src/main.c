/* Includes */
#include "main.h"
#include "stdio.h"
#include "string.h"
#include "gpio.h"
#include "spi.h"
#include "types.h"
#include "defines.h"
#include "timer.h"
#include "adxl375.h"
#include "error.h"
#include "fatfs.h"
#include "debug.h"
#include "time.h"

/* Configuration */
#define USE_SENSOR_1
//#define USE_SENSOR_2
#define USE_SENSOR_3
#define DEBUG_INFO
#define SAMPLES_PER_FILE 1800u // Changing this definition requires adapting the function Write_npy_Header as well.
#define MAX_DATA_FILES_PER_FOLDER 20u

/* Global variables */
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
TIM_HandleTypeDef htim2;

/* Sensor 1 */
volatile BOOL     sensor_1_data_ready = FALSE;
         uint32_t sensor_1_data_ready_time;
         BOOL     write_buf_1_S1      = TRUE;
         uint32_t timestamp_buf_S1_1[SAMPLES_PER_FILE];
         uint32_t timestamp_buf_S1_2[SAMPLES_PER_FILE];
         uint8_t  data_buf_S1_1[6*SAMPLES_PER_FILE];
         uint8_t  data_buf_S1_2[6*SAMPLES_PER_FILE];
         uint16_t sample_cnt_S1 = 0;
         uint32_t file_cnt_S1   = 0;
         uint32_t folder_cnt_S1 = 0;
         char     folder_name_S1[13];
         uint8_t  folder_name_length_S1;

/* Sensor 2 */
volatile BOOL     sensor_2_data_ready = FALSE;
         uint32_t sensor_2_data_ready_time;
         BOOL     write_buf_1_S2      = TRUE;
         uint32_t timestamp_buf_S2_1[SAMPLES_PER_FILE];
         uint32_t timestamp_buf_S2_2[SAMPLES_PER_FILE];
         uint8_t  data_buf_S2_1[6*SAMPLES_PER_FILE];
         uint8_t  data_buf_S2_2[6*SAMPLES_PER_FILE];
         uint16_t sample_cnt_S2 = 0;
         uint32_t file_cnt_S2   = 0;
         uint32_t folder_cnt_S2 = 0;
         char     folder_name_S2[13];
         uint8_t  folder_name_length_S2;

/* Sensor 3 */
volatile BOOL     sensor_3_data_ready = FALSE;
         uint32_t sensor_3_data_ready_time;
         BOOL     write_buf_1_S3      = TRUE;
         uint32_t timestamp_buf_S3_1[SAMPLES_PER_FILE];
         uint32_t timestamp_buf_S3_2[SAMPLES_PER_FILE];
         uint8_t  data_buf_S3_1[6*SAMPLES_PER_FILE];
         uint8_t  data_buf_S3_2[6*SAMPLES_PER_FILE];
         uint16_t sample_cnt_S3 = 0;
         uint32_t file_cnt_S3   = 0;
         uint32_t folder_cnt_S3 = 0;
         char     folder_name_S3[13];
         uint8_t  folder_name_length_S3;

BOOL record_data = FALSE;

/* Function prototypes */
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);

static void Read_Sensor_Data_(const SENSOR sensor, uint8_t* data, const uint16_t sample_cnt) {
    const uint32_t offset = 6u*sample_cnt;

    SPI_Multi_Read(0x32, data+offset, 6u, SPI_INTERFACE_1, sensor);
}

static BOOL Read_Sensor_Data(const SENSOR sensor, const uint32_t timestamp) {
#ifdef USE_SENSOR_1
    if (sensor == SENSOR_1) {
        if (write_buf_1_S1 == TRUE) {
            Read_Sensor_Data_(SENSOR_1, data_buf_S1_1, sample_cnt_S1);
            timestamp_buf_S1_1[sample_cnt_S1] = timestamp;
        } else {
            Read_Sensor_Data_(SENSOR_1, data_buf_S1_2, sample_cnt_S1);
            timestamp_buf_S1_2[sample_cnt_S1] = timestamp;
        }
        if (sample_cnt_S1 == SAMPLES_PER_FILE-1u) {
            sample_cnt_S1 = 0;
            if (write_buf_1_S1 == TRUE) {
                write_buf_1_S1 = FALSE;
            } else {
                write_buf_1_S1 = TRUE;
            }
            return TRUE;
        } else {
            ++sample_cnt_S1;
            return FALSE;
        }
    }
#endif
#ifdef USE_SENSOR_2
    if (sensor == SENSOR_2) {
        if (write_buf_1_S2 == TRUE) {
            Read_Sensor_Data_(SENSOR_2, data_buf_S2_1, sample_cnt_S2);
            timestamp_buf_S2_1[sample_cnt_S2] = timestamp;
        } else {
            Read_Sensor_Data_(SENSOR_2, data_buf_S2_2, sample_cnt_S2);
            timestamp_buf_S2_2[sample_cnt_S2] = timestamp;
        }
        if (sample_cnt_S2 == SAMPLES_PER_FILE-1u) {
            sample_cnt_S2 = 0;
            if (write_buf_1_S2 == TRUE) {
                write_buf_1_S2 = FALSE;
            } else {
                write_buf_1_S2 = TRUE;
            }
            return TRUE;
        } else {
            ++sample_cnt_S2;
            return FALSE;
        }
    }
#endif
#ifdef USE_SENSOR_3
    if (sensor == SENSOR_3) {
        if (write_buf_1_S3 == TRUE) {
            Read_Sensor_Data_(SENSOR_3, data_buf_S3_1, sample_cnt_S3);
            timestamp_buf_S3_1[sample_cnt_S3] = timestamp;
        } else {
            Read_Sensor_Data_(SENSOR_3, data_buf_S3_2, sample_cnt_S3);
            timestamp_buf_S3_2[sample_cnt_S3] = timestamp;
        }
        if (sample_cnt_S3 == SAMPLES_PER_FILE-1u) {
            sample_cnt_S3 = 0;
            if (write_buf_1_S3 == TRUE) {
                write_buf_1_S3 = FALSE;
            } else {
                write_buf_1_S3 = TRUE;
            }
            return TRUE;
        } else {
            ++sample_cnt_S3;
            return FALSE;
        }
    }
#endif
    return FALSE;
}

static void Configure_Sensors(void) {
#ifdef USE_SENSOR_1
    Configure_ADXL375(SENSOR_1);
#endif
#ifdef USE_SENSOR_2
    Configure_ADXL375(SENSOR_2);
#endif
#ifdef USE_SENSOR_3
    Configure_ADXL375(SENSOR_3);
#endif
}

// Taken from: https://github.com/rogersce/cnpy
static BOOL Write_npy_Header_Data(FIL* fil_ptr) {
    UINT bytes_written; FRESULT fres;

    /* Write the header */
    char header[80];
    header[0] = 0x93u;
    strncpy(header+1, "NUMPY", 5);
    header[6] = 0x01u;
    header[7] = 0x00u;
    // Header in little-endian format
    header[8] = 0x46u;
    header[9] = 0x00u;
    strncpy(header+10, "{'descr':'<u1','fortran_order':False,'shape':(1800,6)}               \n", 70);

    fres = f_write(fil_ptr, header, 80u, &bytes_written);
    if ( (fres != FR_OK) | (bytes_written != 80u) ) return FALSE;
}

// Taken from: https://github.com/rogersce/cnpy
static BOOL Write_npy_Header_Timestamps(FIL* fil_ptr) {
    UINT bytes_written; FRESULT fres;

    /* Write the header */
    char header[64];
    header[0] = 0x93u;
    strncpy(header+1, "NUMPY", 5);
    header[6] = 0x01u;
    header[7] = 0x00u;
    // Header in little-endian format
    header[8] = 0x36u;
    header[9] = 0x00u;
    strncpy(header+10, "{'descr':'<u4','fortran_order':False,'shape':(1800,)}\n", 54);

    fres = f_write(fil_ptr, header, 64u, &bytes_written);
    if ( (fres != FR_OK) | (bytes_written != 64u) ) return FALSE;
}

static void Write_Data_Files(const SENSOR sensor) {
    UINT bytes_written; FRESULT fres; FIL fil; uint8_t sensor_number;
    uint8_t file_name_length; char* folder_name; uint8_t folder_name_length;

    // Determine sensor-specific quantities
    uint32_t file_number; uint32_t folder_number; uint8_t* data_buf; uint32_t* timestamps;
    if (sensor == SENSOR_1) {
        file_number   = file_cnt_S1;
        folder_number = folder_cnt_S1;
        if (file_number == MAX_DATA_FILES_PER_FOLDER-1u) {
            file_cnt_S1 = 0u;
            ++folder_cnt_S1;
            folder_name_length_S1 = 3u + sprintf(folder_name_S1+3, "%lu/", folder_cnt_S1);
            folder_name_S1[folder_name_length_S1-1u] = '\0';
            f_mkdir(folder_name_S1);
            folder_name_S1[folder_name_length_S1-1u] = '/';
        } else {
            ++file_cnt_S1;
        }
        folder_name        = folder_name_S1;
        folder_name_length = folder_name_length_S1;
        if (write_buf_1_S1 == TRUE) {
            data_buf   = data_buf_S1_2;
            timestamps = timestamp_buf_S1_2;
        } else {
            data_buf   = data_buf_S1_1;
            timestamps = timestamp_buf_S1_1;
        }
        sensor_number = 1u;
    } else if (sensor == SENSOR_2) {
        file_number   = file_cnt_S2;
        folder_number = folder_cnt_S2;
        if (file_number == MAX_DATA_FILES_PER_FOLDER-1u) {
            file_cnt_S2 = 0u;
            ++folder_cnt_S2;
            folder_name_length_S2 = 3u + sprintf(folder_name_S2+3, "%lu/", folder_cnt_S2);
            folder_name_S2[folder_name_length_S2-1u] = '\0';
            f_mkdir(folder_name_S2);
            folder_name_S2[folder_name_length_S2-1u] = '/';
        } else {
            ++file_cnt_S2;
        }
        folder_name        = folder_name_S2;
        folder_name_length = folder_name_length_S2;
        if (write_buf_1_S2 == TRUE) {
            data_buf   = data_buf_S2_2;
            timestamps = timestamp_buf_S2_2;
        } else {
            data_buf   = data_buf_S2_1;
            timestamps = timestamp_buf_S2_1;
        }
        sensor_number = 2u;
    } else if (sensor == SENSOR_3) {
        file_number   = file_cnt_S3;
        folder_number = folder_cnt_S3;
        if (file_number == MAX_DATA_FILES_PER_FOLDER-1u) {
            file_cnt_S3 = 0u;
            ++folder_cnt_S3;
            folder_name_length_S3 = 3u + sprintf(folder_name_S3+3, "%lu/", folder_cnt_S3);
            folder_name_S3[folder_name_length_S3-1u] = '\0';
            f_mkdir(folder_name_S3);
            folder_name_S3[folder_name_length_S3-1u] = '/';
        } else {
            ++file_cnt_S3;
        }
        folder_name        = folder_name_S3;
        folder_name_length = folder_name_length_S3;
        if (write_buf_1_S3 == TRUE) {
            data_buf   = data_buf_S3_2;
            timestamps = timestamp_buf_S3_2;
        } else {
            data_buf   = data_buf_S3_1;
            timestamps = timestamp_buf_S3_1;
        }
        sensor_number = 3u;
    } else {
        return;
    }

    /* Save the xyz data */
    // Determine the filename
    char xyz_file_name[33]; // "Sx_y/"-> 12 "data_Sz_"-> 8 file_number->7 ".npy"->5
    strcpy(xyz_file_name, folder_name);
    strncpy(xyz_file_name+folder_name_length, "DATA_Sx_", 8u);
    xyz_file_name[folder_name_length+6u] = sensor_number + 48u;
    sprintf(xyz_file_name+folder_name_length+8u, "%u.npy", file_number);
    printf("%s\n",xyz_file_name);

    // Open the file
    fres = f_open(&fil, xyz_file_name, FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
    if (fres != FR_OK) {
        printf("f_open error (%i)\r\n", fres);
        return;
    }

    // Write the npy header to the file
    Write_npy_Header_Data(&fil); // TODO: Use return value

    // Write the data to the file
    fres = f_write(&fil, data_buf, SAMPLES_PER_FILE*6u, &bytes_written);
    if ( (fres != FR_OK) | (bytes_written != SAMPLES_PER_FILE*6u) ) return FALSE;

    // Close the file
    f_close(&fil);

    /* Save the timestamps */
    // Determine the filename
    char ts_file_name[31]; // "Sx_y/"-> 12 "ts_Sx_"-> 6 file_number->7 ".npy"->5
    strcpy(ts_file_name, folder_name);
    strncpy(ts_file_name+folder_name_length, "TS_Sx_", 6u);
    ts_file_name[folder_name_length+4u] = sensor_number + 48u;
    sprintf(ts_file_name+folder_name_length+6u, "%u.npy", file_number);
    printf("%s\n",ts_file_name);

    // Open the file
    fres = f_open(&fil, ts_file_name, FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
    if (fres != FR_OK) {
        printf("f_open error (%i)\r\n", fres);
        return;
    }

    // Write the npy header to the file
    Write_npy_Header_Timestamps(&fil); // TODO: Use the return value

    // Write the data to the file
    fres = f_write(&fil, (uint8_t*) timestamps, SAMPLES_PER_FILE*4u, &bytes_written);
    if ( (fres != FR_OK) | (bytes_written != SAMPLES_PER_FILE*4u) ) return FALSE;

    // Close the file
    f_close(&fil);
}

int main(void) {
    /* Reset of all peripherals, initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_SPI1_Init();
    MX_SPI2_Init();

    /* Initialize TIM2 */
    TIM2_Init();

    /* Configure the ADXL375 sensors */
    Configure_Sensors();

    /* Initialize FatFs */
    MX_FATFS_Init();
    HAL_Delay(1000); // Required for proper initialization of the SD card

    /* Initialize FatFs variables */
    FATFS FatFs; // FatFs handle

    /* Initialize the last button press time as the current time */
    uint32_t time_stamp = TIM2->CNT;

    /* Read all sensors to initialize variables and clear interrupts */
#ifdef USE_SENSOR_1
    Read_Sensor_Data(SENSOR_1, 0u);
    uint32_t s1_last_read_time = TIM2->CNT;
             sample_cnt_S1     = 0;
#endif
#ifdef USE_SENSOR_2
    Read_Sensor_Data(SENSOR_2, 0u);
    uint32_t s2_last_read_time = TIM2->CNT;
             sample_cnt_S2     = 0;
#endif
#ifdef USE_SENSOR_3
    Read_Sensor_Data(SENSOR_3, 0u);
    uint32_t s3_last_read_time = TIM2->CNT;
             sample_cnt_S3     = 0;
#endif

    /* Initialize the sensor message timeout counts */
    uint32_t timeout_cnt_1 = 0;
    uint32_t timeout_cnt_2 = 0;
    uint32_t timeout_cnt_3 = 0;

    /* Initialize debug variables */
#ifdef DEBUG_INFO
    uint32_t last_debug_info_time = TIM2->CNT;
#endif

    while (1) {
        /* Check if data shall be recorded or not */
        if ((GPIO_Read(BUTTON_GPIO_Port, BUTTON_Pin) == TRUE) & (Get_us_Since(time_stamp) > 300000u)) {
            if (record_data == TRUE) {
                record_data = FALSE;
                GPIO_Reset(LED_BLUE_GPIO_Port, LED_BLUE_Pin);

                // Unmount the SD card
                f_mount(NULL, "", 0);
            } else {
                record_data = TRUE;
#ifdef USE_SENSOR_1
                sample_cnt_S1 = 0;
#endif
#ifdef USE_SENSOR_2
                sample_cnt_S2 = 0;
#endif
#ifdef USE_SENSOR_3
                sample_cnt_S3 = 0;
#endif
                GPIO_Set(LED_BLUE_GPIO_Port, LED_BLUE_Pin);

                // Mount the SD card
                FRESULT fres = f_mount(&FatFs, "", 1); //1=mount now
                if (fres != FR_OK) {
#ifdef DEBUG_INFO
                    printf("f_mount error (%i)\r\n", fres);
#endif
                    Error_Handler();
                }

                // Create the initial folders
#ifdef USE_SENSOR_1
                f_mkdir("S1_0");
                strcpy(folder_name_S1, "S1_0/");
                folder_name_length_S1 = 5u;
#endif
#ifdef USE_SENSOR_2
                f_mkdir("S2_0");
                strcpy(folder_name_S2, "S2_0/");
                folder_name_length_S2 = 5u;
#endif
#ifdef USE_SENSOR_3
                f_mkdir("S3_0");
                strcpy(folder_name_S3, "S3_0/");
                folder_name_length_S3 = 5u;
#endif
            }
            time_stamp = TIM2->CNT;
        }

        /* Read newly available sensor data */
#ifdef USE_SENSOR_1
        BOOL S1_buf_full = FALSE;
        if (sensor_1_data_ready == TRUE) {
            sensor_1_data_ready = FALSE;
            s1_last_read_time   = TIM2->CNT;
            S1_buf_full         = Read_Sensor_Data(SENSOR_1, sensor_1_data_ready_time);
        }
#endif
#ifdef USE_SENSOR_2
        BOOL S2_buf_full = FALSE;
        if (sensor_2_data_ready == TRUE) {
            sensor_2_data_ready = FALSE;
            s2_last_read_time   = TIM2->CNT;
            S2_buf_full         = Read_Sensor_Data(SENSOR_2, sensor_2_data_ready_time);
        }
#endif
#ifdef USE_SENSOR_3
        BOOL S3_buf_full = FALSE;
        if (sensor_3_data_ready == TRUE) {
            sensor_3_data_ready = FALSE;
            s3_last_read_time   = TIM2->CNT;
            S3_buf_full         = Read_Sensor_Data(SENSOR_3, sensor_3_data_ready_time);
        }
#endif

        /* Check that all sensors are read consistently */
        uint32_t time_now = TIM2->CNT;
#ifdef USE_SENSOR_1
        if (Check_Sensor_Timeout(s1_last_read_time,time_now) == TRUE) {
            s1_last_read_time = time_now;
            S1_buf_full       = Read_Sensor_Data(SENSOR_1, TIM2->CNT);
            time_now          = TIM2->CNT;
            ++timeout_cnt_1;
        }
#endif
#ifdef USE_SENSOR_2
        if (Check_Sensor_Timeout(s2_last_read_time,time_now) == TRUE) {
            s2_last_read_time = time_now;
            S2_buf_full       = Read_Sensor_Data(SENSOR_2, TIM2->CNT);
            time_now          = TIM2->CNT;
            ++timeount_cnt_2;
        }
#endif
#ifdef USE_SENSOR_3
        if (Check_Sensor_Timeout(s3_last_read_time,time_now) == TRUE) {
            s3_last_read_time = time_now;
            S3_buf_full       = Read_Sensor_Data(SENSOR_3, TIM2->CNT);
            time_now          = TIM2->CNT;
            ++timeout_cnt_3;
        }
#endif

        /* Check if data should be written to the SD card */
#ifdef USE_SENSOR_1
        if (S1_buf_full & record_data) {
#ifdef DEBUG_INFO
            printf("Writing data of sensor 1.\n");
#endif
            Write_Data_Files(SENSOR_1);
        }
#endif
#ifdef USE_SENSOR_2
        if (S2_buf_full & record_data) {
#ifdef DEBUG_INFO
            printf("Writing data of sensor 2.\n");
#endif
            Write_Data_Files(SENSOR_2);
        }
#endif
#ifdef USE_SENSOR_3
        if (S3_buf_full & record_data) {
#ifdef DEBUG_INFO
            printf("Writing data of sensor 3.\n");
#endif
            Write_Data_Files(SENSOR_3);
        }
#endif

        /* Print debug information */
#ifdef DEBUG_INFO
        if (Get_us_Since(last_debug_info_time) >= 1000000u) {
            last_debug_info_time = TIM2->CNT;
            printf("Timeout count: %lu,%lu,%lu\n", timeout_cnt_1, timeout_cnt_2, timeout_cnt_3);
            /*
            uint8_t out_1, out_2, out_3;
            SPI_Read(INT_ENABLE_REG_ADDR, &out_1, SPI_INTERFACE_1, SENSOR_1);
            //SPI_Read(INT_ENABLE_REG_ADDR, &out_2, SPI_INTERFACE_1, SENSOR_2);
            SPI_Read(INT_ENABLE_REG_ADDR, &out_3, SPI_INTERFACE_1, SENSOR_3);
            printf("%d,%d,%d\n",out_1,out_2,out_3);
            SPI_Read(POWER_CTL_REG_ADDR, &out_1, SPI_INTERFACE_1, SENSOR_1);
            //SPI_Read(POWER_CTL_REG_ADDR, &out_2, SPI_INTERFACE_1, SENSOR_2);
            SPI_Read(POWER_CTL_REG_ADDR, &out_3, SPI_INTERFACE_1, SENSOR_3);
            printf("%d,%d,%d\n",out_1,out_2,out_3);
            last += 1000000;*/
        }
#endif
    }

    /*
    //Let's get some statistics from the SD card
    DWORD free_clusters, free_sectors, total_sectors;

    FATFS* getFreeFs;

    fres = f_getfree("", &free_clusters, &getFreeFs);
    if (fres != FR_OK) {
        printf("f_getfree error (%i)\r\n", fres);
    while(1);
    }

    //Formula comes from ChaN's documentation
    total_sectors = (getFreeFs->n_fatent - 2) * getFreeFs->csize;
    free_sectors = free_clusters * getFreeFs->csize;

    printf("SD card stats:\r\n%10lu KiB total drive space.\r\n%10lu KiB available.\r\n", total_sectors / 2, free_sectors / 2);

    //Now let's try to open file "test.txt"
    fres = f_open(&fil, "test.txt", FA_READ);
    if (fres != FR_OK) {
        printf("f_open error (%i)\r\n", fres);
    while(1);
    }
    printf("I was able to open 'test.txt' for reading!\r\n");

    //Read 30 bytes from "test.txt" on the SD card
    BYTE readBuf[30];

    //We can either use f_read OR f_gets to get data out of files
    //f_gets is a wrapper on f_read that does some string formatting for us
    TCHAR* rres = f_gets((TCHAR*)readBuf, 30, &fil);
    if(rres != 0) {
        printf("Read string from 'test.txt' contents: %s\r\n", readBuf);
    } else {
        printf("f_gets error (%i)\r\n", fres);
    }

    //Be a tidy kiwi - don't forget to close your file!
    f_close(&fil);


*/







    /* Debug*/
    uint8_t out_1, out_2, out_3;
    SPI_Read(INT_ENABLE_REG_ADDR, &out_1, SPI_INTERFACE_1, SENSOR_1);
    //SPI_Read(INT_ENABLE_REG_ADDR, &out_2, SPI_INTERFACE_1, SENSOR_2);
    SPI_Read(INT_ENABLE_REG_ADDR, &out_3, SPI_INTERFACE_1, SENSOR_3);
    printf("%d,%d,%d\n",out_1,out_2,out_3);

    // Open the file system
    //fres = f_mount(&FatFs, "", 1); //1=mount now
    //if (fres != FR_OK) {
    //    printf("f_mount error (%i)\r\n", fres);
     //   Error_Handler();
    //}

    /* Infinite loop */
    //uint16_t sec_cnt = 0;
    while (1) {





        //uint8_t dev_id;
        //SPI_Read(0x00, &dev_id, SPI_INTERFACE_1, SENSOR_1);
        //if (dev_id != 229u) printf("Error\n");
        //printf("Ok\n");
        //HAL_Delay(1000);



    }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;//50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;//RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
        Error_Handler();
    }
}

static void MX_SPI1_Init(void) {
    // Configure the peripheral
    hspi1.Instance = SPI1;
    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
    hspi1.Init.NSS = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;//SPI_BAUDRATEPRESCALER_4;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&hspi1) != HAL_OK) {
        Error_Handler();
    }

    // Enable the SPI interface
    SPI1->CR1 |= SPI_CR1_SPE;
}

static void MX_SPI2_Init(void) {
    // Configure the peripheral
    hspi2.Instance = SPI2;
    hspi2.Init.Mode = SPI_MODE_MASTER;
    hspi2.Init.Direction = SPI_DIRECTION_2LINES;
    hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi2.Init.NSS = SPI_NSS_SOFT;
    hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
    hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi2.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&hspi2) != HAL_OK) {
        Error_Handler();
    }

    // Enable the SPI interface
    SPI2->CR1 |= SPI_CR1_SPE;
}

// TODO: Optimize this function
static void MX_GPIO_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /* Configure GPIO pin Output Level */
    // TODO: Remove this with non-HAL calls
    HAL_GPIO_WritePin(SS_S1_GPIO_Port, SS_S1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(SS_S2_GPIO_Port, SS_S2_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(SS_S3_GPIO_Port, SS_S3_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(SS_SD_GPIO_Port, SS_SD_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);

    /* Configure GPIO pins : SS_S1_Pin */
    GPIO_InitStruct.Pin   = SS_S1_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SS_S1_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : SS_S2_Pin */
    GPIO_InitStruct.Pin   = SS_S2_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SS_S2_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : SS_S3_Pin */
    GPIO_InitStruct.Pin   = SS_S3_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SS_S3_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : SS_SD_Pin */
    GPIO_InitStruct.Pin   = SS_SD_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SS_SD_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : DR_S1_Pin */
    GPIO_InitStruct.Pin  = DR_S1_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DR_S1_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : DR_S2_Pin */
    GPIO_InitStruct.Pin  = DR_S2_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DR_S2_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : DR_S3_Pin */
    GPIO_InitStruct.Pin  = DR_S3_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DR_S3_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : BUTTON_Pin */
    GPIO_InitStruct.Pin   = BUTTON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : LED_RED_Pin */
    GPIO_InitStruct.Pin   = LED_RED_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_RED_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pin : LED_BLUE_Pin */
    GPIO_InitStruct.Pin   = LED_BLUE_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_RED_GPIO_Port, &GPIO_InitStruct);

    /* EXTI interrupt initialization */
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

    HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void EXTI9_5_IRQHandler(void) {
    const uint32_t EXTI_PR = EXTI->PR;
    const uint32_t time    = TIM2->CNT;
    if (EXTI_PR & EXTI_PR_PR7) {
        EXTI->PR           |= EXTI_PR_PR7;
#ifdef USE_SENSOR_1
        sensor_3_data_ready      = TRUE;
        sensor_3_data_ready_time = time;
#endif
    }
}

void EXTI15_10_IRQHandler(void) {
    const uint32_t EXTI_PR = EXTI->PR;
    const uint32_t time    = TIM2->CNT;
    if (EXTI_PR & EXTI_PR_PR15) {
        EXTI->PR           |= EXTI_PR_PR15;
#ifdef USE_SENSOR_1
        sensor_1_data_ready      = TRUE;
        sensor_1_data_ready_time = time;
#endif
    }
    if (EXTI_PR & EXTI_PR_PR10) {
        EXTI->PR           |= EXTI_PR_PR10;
#ifdef USE_SENSOR_2
        sensor_2_data_ready      = TRUE;
        sensor_2_data_ready_time = time;
#endif
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
