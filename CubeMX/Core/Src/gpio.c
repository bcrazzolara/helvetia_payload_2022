/*
 * gpio.c
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

#include "gpio.h"

void GPIO_Set(GPIO_TypeDef* port, const uint16_t GPIO_Pin) {
    port->BSRR = GPIO_Pin;
}

void GPIO_Reset(GPIO_TypeDef* port, const uint16_t GPIO_Pin) {
    port->BSRR = (uint32_t) GPIO_Pin << 16U;
}

BOOL GPIO_Read(GPIO_TypeDef* port, const uint16_t GPIO_Pin) {
    if ((port->IDR & GPIO_Pin) != 0u) {
        return TRUE;
    } else {
        return FALSE;
    }
}
