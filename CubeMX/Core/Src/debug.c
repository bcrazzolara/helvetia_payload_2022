/*
 * debug.c
 *
 *  Created on: May 30, 2022
 *      Author: Felix Crazzolara
 */

#include "debug.h"
#include "stm32f4xx_hal.h"

int __io_putchar(int ch) {
    ITM_SendChar(ch);
    return(ch);
}

int _write(int file, char *ptr, int len) {
    int DataIdx;
    for (DataIdx = 0; DataIdx < len; DataIdx++) {
        __io_putchar(*ptr++);
    }
    return len;
}
