/*
 * adxl375.c
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "adxl375.h"
#include "spi.h"
#include "defines.h"
#include "error.h"

BOOL Check_Sensor_Timeout(const uint32_t last_read, const uint32_t time_now) {
    if (time_now < last_read) { // time_now + 100'000'000 >= last_read
        // Check if the time since the last read is more than 1/3200Hz*2=312.5us*2=625us 1562.5
        return (time_now+100000000u)-last_read >= 1562u ? TRUE : FALSE;
    } else { // time_now >= last_read
        // Check if the time since the last read is more than 1/3200Hz*2=312.5us*2=625us
        return time_now-last_read >= 1562u ? TRUE : FALSE;
    }
}

void Configure_ADXL375(const SENSOR sensor) {
    // Check the sensor ID
    uint8_t dev_id = 0u;
    SPI_Read(DEVID_REG_ADDR, &dev_id, SPI_INTERFACE_1, sensor);
    if (dev_id != 229u) Error_Handler();

    // Enable sensor interrupts
    SPI_Write(INT_ENABLE_REG_ADDR, 0x80u, SPI_INTERFACE_1, sensor);

    // Set the data output rate to 3.2kHz
    SPI_Write(BW_RATE_REG_ADDR, 0x0Fu, SPI_INTERFACE_1, sensor);

    if (sensor == SENSOR_1) {
        uint8_t val;
        SPI_Read(INT_MAP_REG_ADDR, &val, SPI_INTERFACE_1, sensor);
        val |= 0x80;
        SPI_Write(INT_MAP_REG_ADDR, val, SPI_INTERFACE_1, sensor);
    }

    // Put the sensor into measurement mode
    SPI_Write(POWER_CTL_REG_ADDR, 0x08u, SPI_INTERFACE_1, sensor);
    uint8_t val;
    SPI_Read(POWER_CTL_REG_ADDR, &val, SPI_INTERFACE_1, sensor);
    if (val != 0x08u) Error_Handler();
}
