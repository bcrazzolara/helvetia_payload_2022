/*
 * error.c
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "stm32f4xx_hal.h"
#include "error.h"
#include "defines.h"

void Error_Handler(void) {
    __disable_irq();
    while (1) {
        // TODO: Don't use the HAL library
        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
        for (uint32_t i = 0; i < 1000000; ++i) {};
        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
        for (uint32_t i = 0; i < 1000000; ++i) {};
    }
}
