/*
 * spi.c
 *
 *  Created on: May 22, 2022
 *      Author: Felix Crazzolara
 */

/* Includes */
#include "spi.h"
#include "gpio.h"
#include "defines.h"

void SPI_Tranceive(const uint8_t* pTxData, uint8_t* pRxData, const uint16_t size, SPI_TypeDef* spi) {
    // TODO: Make sure RXNE bit is not set
    for (uint16_t dIdx = 0u; dIdx < size; ++dIdx) {
        while ((spi->SR & SPI_SR_TXE) == 0u) {}
        spi->DR = pTxData[dIdx];
        while ((spi->SR & SPI_SR_RXNE) == 0u) {}
        pRxData[dIdx] = spi->DR;
    }
}

void SPI_Read_Write(uint8_t reg, const uint8_t val, uint8_t* ret, const SPI_INTERFACE spi_interface, const SENSOR sensor,
                    const BOOL read) {
    // Determine the SPI interface as well as the slave select GPIO port and pin
    SPI_TypeDef*  spi;
    GPIO_TypeDef* ss_port;
    uint16_t      ss_pin;
    if (spi_interface == SPI_INTERFACE_1) {
        // This is ADXL375 specific
        if (read == TRUE) reg |= 0x80;

        spi = SPI1;
        switch(sensor) {
            case SENSOR_1: ss_port = SS_S1_GPIO_Port; ss_pin = SS_S1_Pin; break;
            case SENSOR_2: ss_port = SS_S2_GPIO_Port; ss_pin = SS_S2_Pin; break;
            case SENSOR_3: ss_port = SS_S3_GPIO_Port; ss_pin = SS_S3_Pin; break;
            default: Error_Handler();
        }
    } else if (spi_interface == SPI_INTERFACE_2) {
        spi = SPI2;
        // TODO: Implement this
    } else {
        return;
    }

    const uint16_t size      = 2;
    uint8_t        TxData[2];
                   TxData[0] = reg;
                   TxData[1] = val;
    uint8_t        RxData[2];

    GPIO_Reset(ss_port, ss_pin);
    SPI_Tranceive(TxData, RxData, size, spi);
    GPIO_Set(ss_port, ss_pin);

    if (read == TRUE) *ret = RxData[1];
}

void SPI_Read(const uint8_t reg, uint8_t* ret, const SPI_INTERFACE spi_interface, const SENSOR sensor) {
    SPI_Read_Write(reg, 0x00, ret, spi_interface, sensor, TRUE);
}

void SPI_Multi_Read(uint8_t reg0, uint8_t* ret, const uint8_t num_regs, const SPI_INTERFACE spi_interface,
                    const SENSOR sensor) {
    // Determine the SPI interface as well as the slave select GPIO port and pin
    SPI_TypeDef*  spi;
    GPIO_TypeDef* ss_port;
    uint16_t      ss_pin;
    if (spi_interface == SPI_INTERFACE_1) {
        // This is ADXL375 specific
        reg0 |= 0xC0;

        spi = SPI1;
        switch(sensor) {
            case SENSOR_1: ss_port = SS_S1_GPIO_Port; ss_pin = SS_S1_Pin; break;
            case SENSOR_2: ss_port = SS_S2_GPIO_Port; ss_pin = SS_S2_Pin; break;
            case SENSOR_3: ss_port = SS_S3_GPIO_Port; ss_pin = SS_S3_Pin; break;
            default: Error_Handler();
        }
    } else if (spi_interface == SPI_INTERFACE_2) {
        spi = SPI2;
        // TODO: Implement this
    } else {
        return;
    }

    GPIO_Reset(ss_port, ss_pin);
    for (uint8_t reg_cnt = 0; reg_cnt < num_regs+1u; ++reg_cnt) {
        if (reg_cnt == 0u) {
            uint8_t out;
            SPI_Tranceive(&reg0, &out, 1u, spi);
        } else {
            uint8_t zero = 0u;
            SPI_Tranceive(&zero, ret+(reg_cnt-1u), 1u, spi);
        }
    }
    GPIO_Set(ss_port, ss_pin);
}

void SPI_Write(const uint8_t reg, const uint8_t val, const SPI_INTERFACE spi_interface, const SENSOR sensor) {
    SPI_Read_Write(reg, val, NULL, spi_interface, sensor, FALSE);
}
