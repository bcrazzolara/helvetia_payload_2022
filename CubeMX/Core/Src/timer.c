/*
 * timer.c
 *
 *  Created on: May 23, 2022
 *      Author: Felix Crazzolara
 */

#include "timer.h"
#include "stm32f4xx_hal.h"

void TIM2_Init(void) {
    // Enable the clock for the TIM2 peripheral
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

    // Restart the timer once it reaches 99'999'999
    TIM2->ARR = 99999999u;

    //-> According to dm00037051.pdf, TIM2 is on APB1
    //-> APB1 has a frequency of 42MHz, but timer clocks are twice as fast
    //-> Let the counter run with 1MHz
    TIM2->PSC = 83u;

    // Use counter enable (CEN) as trigger timer trigger
    TIM2->CR2 |= TIM_CR2_MMS_0;

    // Enable the timer and enable auto-reload
    TIM2->CR1 |= TIM_CR1_CEN | TIM_CR1_ARPE;
}
