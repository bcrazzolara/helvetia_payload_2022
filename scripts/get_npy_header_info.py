# npy data format definition:
# https://github.com/numpy/numpy/blob/067cb067cb17a20422e51da908920a4fbb3ab851/doc/neps/nep-0001-npy-format.rst

# Configuration
samples_per_file = 1800

if __name__ == "__main__":
    #--- Data ---#
    header = "\x93NUMPY"

    dict_ = "{'descr':'"

    # STM32 devices are little endian
    # https://electronics.stackexchange.com/questions/183021/stm32f103c8xx-big-or-small-endian
    dict_ += f"<u1','fortran_order':False,'shape':({str(samples_per_file)},6)"+"}"

    mod = (10+len(dict_)+1) % 16
    if mod != 0:
        dict_ += ' '*(16-mod)
    print('----- Data -----')
    print(f"The total header length is {10+len(dict_)+1}bytes.")
    print(f"The dict length is {len(dict_)}bytes.")
    print(f"dict: \"{dict_}\"")

    #--- Timestamps ---#
    header = "\x93NUMPY"

    dict_ = "{'descr':'"

    # STM32 devices are little endian
    # https://electronics.stackexchange.com/questions/183021/stm32f103c8xx-big-or-small-endian
    dict_ += f"<u4','fortran_order':False,'shape':({str(samples_per_file)},)"+"}"

    mod = (10+len(dict_)+1) % 16
    if mod != 0:
        dict_ += ' '*(16-mod)
    print('----- Timestamps -----')
    print(f"The total header length is {10+len(dict_)+1}bytes.")
    print(f"The dict length is {len(dict_)}bytes.")
    print(f"dict: \"{dict_}\"")
