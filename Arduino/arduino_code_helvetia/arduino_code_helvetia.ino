#include <SPI.h>

// maxiumum clock divider is 329kHz, 

// Pin definitions
#define SS 7
#define DR 8
#define INT_ENABLE_REG_ADDR 0x2E
#define BW_RATE 0x2C

// Global variables
volatile bool dataReady = false;

void writeRegister(uint8_t regAddress, uint8_t value) {
  //Set Chip Select pin low to signal the beginning of an SPI packet.
  digitalWrite(SS, LOW);
  //Transfer the register address over SPI.
  SPI.transfer(regAddress);
  //Transfer the desired register value over SPI.
  SPI.transfer(value);
  //Set the Chip Select pin high to signal the end of an SPI packet.
  digitalWrite(SS, HIGH);
}

void configSPI() {
    SPISettings IMUSettings(500000, MSBFIRST, SPI_MODE3); // SPI frequency, for one bit transfer, 
    SPI.beginTransaction(IMUSettings);
}

void dataReadyIntHandler() {
    dataReady = true;
}

void _multiReadRegister(uint8_t regAddress, uint8_t values[], uint8_t numberOfBytes) {
  // Since we're performing a read operation, the most significant bit of the register address should be set.
  regAddress |= 0x80;

  // set the multi-read byte if required
  if (numberOfBytes > 1) {
    regAddress |= 0x40;
  }
  
  // set the Chip select pin low to start an SPI packet.
  digitalWrite(SS, LOW);
  // transfer the starting register address that needs to be read.
  SPI.transfer(regAddress);
  
  // read the data
  for(int i=0; i<numberOfBytes; i++){
    values[i] = SPI.transfer(0x00);
  }

  //Set the Chips Select pin high to end the SPI packet.
  digitalWrite(SS, HIGH);
}

uint8_t readRegister(uint8_t regAddress) {
  uint8_t data[1];
  _multiReadRegister(regAddress, data, 1);
  return data[0];
}

/*
AccelReading getXYZ()
{
  uint8_t data[6];
  _multiReadRegister(ADXL375_REG_DATAX0, data, 6);

  AccelReading xyz;
  xyz.init(
    data[0] | data[1]<<8,
    data[2] | data[3]<<8,
    data[4] | data[5]<<8,
    ADXL375_XYZ_READ_SCALE_FACTOR
  );
  
  return xyz;
}*/
/*
void foo_write(){
  uint8_t ret = SPI.transfer(0xCC);
  //Serial.println((int)ret);
}*/

void setup() {
    // Initialize the serial connection
    Serial.begin(115200); // Initialize serial output via USB

    // Configure GPIO pins
    pinMode(SS, OUTPUT);
    digitalWrite(SS, HIGH);
    pinMode(DR, INPUT);

    // Initialize the SPI communication
    SPI.begin();
    configSPI();

    // Give the IMU some time to start up
    delay(100);

    // Configure the data ready stuff
    writeRegister(INT_ENABLE_REG_ADDR,0x80);
    writeRegister(0x2D,0x08);
    writeRegister(BW_RATE, 0x0F); // definition of the data rate bits

    // Configure interrupts
    attachInterrupt(digitalPinToInterrupt(DR), dataReadyIntHandler, RISING);
    
    /*
    regWrite(MSC_CTRL, 0x06);  // Enable Data Ready, set polarity
    delay(20); 
    regWrite(SENS_AVG, 0x402); // Set digital filter
    delay(20);
    regWrite(SMPL_PRD, 0x01), // Disable decimation
    delay(20);
    */
}

// Main loop

void loop() {
    if (dataReady) {
        dataReady = false;
        //AccelReading ADXL375::getXYZ;
        Serial.println("Yes");
        Serial.println((int) readRegister(0x32));
        Serial.println((int) readRegister(0x30));
    }
    
    //uint8_t ret = readRegister(0x30);
    //Serial.println("----");
    //if (ret == 0x00) Serial.println("_");
    //else Serial.println((int) ret);

    
   
    //foo_write();
    //delay(100);
    //regWrite(MSC_CTRL, 0x06);
    //byteBurst();
    // Nothing to do here! The program is entirely interrupt-driven!
}
